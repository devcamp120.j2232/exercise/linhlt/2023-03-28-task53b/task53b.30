import s50.Customer;
import s50.Invoice;
public class App {
    public static void main(String[] args) throws Exception {
        //task 4
        Customer customer1 = new Customer(1, "Linh", 40);
        Customer customer2 = new Customer(2, "Minh", 10);
        System.out.println(customer1);
        System.out.println(customer2);
        //task 5
        Invoice invoice1 = new Invoice(1001, customer1, 1000000 );
        Invoice invoice2 = new Invoice(1002, customer2, 500000 );
        System.out.println(invoice1);
        System.out.println("Invoice 1 amount after discount: " + invoice1.getAmountAfterDiscount());
        System.out.println(invoice2);
        System.out.println("Invoice 2 amount after discount: " + invoice2.getAmountAfterDiscount());
    }
}
